//Crear una clase que permita ingresar valores enteros por teclado y nos muestre la tabla de multiplicar de dicho valor. Finalizar el programa al ingresar el -1.

using System;

class MainClass 
{
  public void cargar()
  {
    int valor,multi=1, j=0;;
    do{
      Console.Write("Ingresar valores: ");
      valor= int.Parse(Console.ReadLine());
      for (int i=0; i<11; i++)
      {
        multi= i*valor;
        Console.WriteLine("{0} x {1} = {2}" ,valor,i,multi);
      }
      
    }while (valor!= -1);
    Console.ReadKey();
  }
  public static void Main (string[] args) 
  {
    MainClass p= new MainClass();
    p.cargar();
  }
}