//Un programa que pida al usuario 5 números reales (pista: necesitarás un array de "float") y luego los muestre en el orden contrario al que se introdujeron.

using System;

class MainClass 
{
  public static void Main (string[] args) 
  {
    float[] valor= new float[5];
    
    for (int f=0 ; f<5 ; f++)
    {
      Console.WriteLine("Ingrese numero real: ");
      valor[f]= float.Parse(Console.ReadLine());
    }
    for (int f=4; f>=0; f--)
    {
      Console.WriteLine("Valor: " + valor[f]);
    }

  }
}