//Un programa que te pida tu nombre y lo muestre en pantalla separando cada letra de la siguiente con un espacio. Por ejemplo, si tu nombre es "Juan", debería aparecer en pantalla "J u a n".


using System;

class MainClass 
{
  public static void Main (string[] args) 
  {
    Console.Write("Ingrese nombre: ");
    string nombre = Console.ReadLine();
    foreach(char letras in nombre)
    {
      Console.WriteLine(letras);
    }
  }
}