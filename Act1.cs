//Un programa que pida al usuario 4 números, los memorice (utilizando un array), calcule su media aritmética y después muestre en pantalla la media y los datos tecleados.

using System;

class MainClass 
{

  public static void Main (string[] args) 
  {
    int[] valor= new int[4];
    double suma=0; 
    
    for (int f=0 ; f<4 ; f++)
    {
      Console.WriteLine("Ingrese valor: ");
      valor[f]= int.Parse(Console.ReadLine());
      suma+= valor[f];
      
    }
    double result= suma/4;
    Console.WriteLine("Media Aritmetica: " + result);

    for (int f=0; f<4; f++)
    {
      Console.WriteLine("Valor: " + valor[f]);
    } 
  }
}