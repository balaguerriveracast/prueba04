//Crear un programa que defina un array de 5 elementos de tipo float que representen las alturas de 5 personas.Obtener el promedio de las mismas. Contar cuántas personas son más altas que el promedio y cuántas más bajas.


using System;

class MainClass 
{
  public static void Main (string[] args) 
  {
    float[] altura= new float[5];
    for (int i=0; i<5; i++)
    {
      Console.Write("Ingrese altura: ");
      altura[i]= float.Parse(Console.ReadLine());
    }

    float suma=0, pro;
    for (int i=0; i<5; i++)
    {
      suma+=altura[i];
    }
    pro=suma/5;
    Console.WriteLine("Promedio: " +pro);

    int mayores=0, menores=0;
    for (int i=0; i<5; i++)
    {
      if(altura[i]>pro)
      {
        mayores++;
      }
      if(altura[i]<pro)
      {
        menores++;
      }
    }
    Console.WriteLine("Personas mayores: " +mayores);
    Console.WriteLine("Personas menores: " +menores);

  }
}